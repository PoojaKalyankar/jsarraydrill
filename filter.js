function filter(items,cb){
    let ansArray=[];
    for(let index=0;index<items.length;index++){
        if(cb(items[index])){
            ansArray.push(items[index]);
        }
    }
    return ansArray;
}

module.exports=filter;
function find(items,cb){
    for(let index=0;index<items.length;index++){
        if(cb(items[index])){
            return items[index];
        }
    }
}

module.exports=find;
function reduce(items,cb,startingValue){
    for(let index=0;index<items.length;index++){
        startingValue=cb(startingValue,items[index]);
    }
    return startingValue;
}

module.exports=reduce;
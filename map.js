function map(items,cb){
    let ansArray=[];
    for(let index=0;index<items.length;index++){
      ansArray.push(cb(items,index));
    }
    return ansArray;
}

module.exports=map;